<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Simple laravel CRUD with Resource Controller
### Things todo list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/simple-laravel-crud-with-resource-controllers.git`
2. Go inside the folder: `cd simple-laravel-crud-with-resource-controllers`
3. Run `composer install`
4. Run `cp .env.example .env`
5. Put your database name and credential on it
6. Run `php artisan migrate`
7. Run `php artisan serve`
8. Open your favorite browser: http://localhost:8000/nerds 

### Screen shot

Home Page

![Home Page](img/list.png "Home Page")

Create New Nerd

![Create New Nerd](img/create.png "Create New Nerd")

Show Nerd

![Show Nerd](img/show.png "Show Nerd")

Edit Nerd

![Edit Nerd](img/edit.png "Edit Nerd")
